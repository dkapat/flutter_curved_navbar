import 'package:flutter/material.dart';

import 'pages/navBarTwo.dart';
import 'pages/navBarOne.dart';
import 'pages/navBarThree.dart';
import 'pages/navBarFour.dart';
import 'pages/navBarFive.dart';
import 'pages/navBarSix.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Colors.red,
        primaryColor: Colors.red,
      ),
      title: 'Flutter Demo',
      home: NavBarOne(),
      //home: NavBarTwo(),
      //home: NavBarThree(),
      //home: NavBarFour(),
      //home: NavBarFive(),
      //home: NavBarSix(),
    );
  }
}