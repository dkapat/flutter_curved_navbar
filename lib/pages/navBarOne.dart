import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

import 'chat.dart';
import 'dashboard.dart';
import 'profile.dart';
import 'settings.dart';

class NavBarOne extends StatefulWidget {
  @override
  _NavBarOneState createState() => _NavBarOneState();
}

class _NavBarOneState extends State<NavBarOne> {
  int currentTab = 0;
  final List<Widget> screens = [
    Dashboard(),
    Chat(),
    Profile(),
    Settings(),
  ];

  Widget currentScreen = Dashboard();
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Flutter App',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        color: Colors.deepOrange,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'This is just a demo app',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        color: Colors.white,
        backgroundColor: Colors.deepOrange,
        buttonBackgroundColor: Colors.white,
        height: 50.0,
        items: <Widget>[
          Icon(Icons.verified_user, size:20, color: Colors.black,),
          Icon(Icons.add, size:20, color: Colors.black,),
          Icon(Icons.list, size:20, color: Colors.black,),
          Icon(Icons.favorite, size:20, color: Colors.black,),
          Icon(Icons.exit_to_app, size:20, color: Colors.black,),
        ],
        animationDuration: Duration(milliseconds: 200),
        animationCurve: Curves.bounceInOut,
        index: 2,
        onTap: (index){
          debugPrint("Current Index is $index");
        },
      ),
    );
  }
}
