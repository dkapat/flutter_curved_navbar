import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';

class NavBarFive extends StatefulWidget {
  @override
  _NavBarFiveState createState() => _NavBarFiveState();
}

class _NavBarFiveState extends State<NavBarFive> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fancy Bottom Navigation'),
      ),
      bottomNavigationBar: FancyBottomNavigation(
        tabs: [
          TabData(iconData: Icons.home, title: "Home"),
          TabData(iconData: Icons.search, title: "Search"),
          TabData(iconData: Icons.person, title: "Account"),
          TabData(iconData: Icons.shopping_cart, title: "Basket"),
        ],
        onTabChangedListener: (position){
          setState(() {
            //currentPage = position;
          });
        },
      ),
    );
  }
}
