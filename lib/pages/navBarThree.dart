import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'chat.dart';
import 'dashboard.dart';
import 'profile.dart';
import 'settings.dart';

class NavBarThree extends StatefulWidget {
  @override
  _NavBarThreeState createState() => _NavBarThreeState();
}

class _NavBarThreeState extends State<NavBarThree> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: Text('Bottom NavBar App'),
      ),
      bottomNavigationBar: BottomNavyBar(),
    );
  }
}

class BottomNavyBar extends StatefulWidget {
  @override
  _BottomNavyBarState createState() => _BottomNavyBarState();
}

class _BottomNavyBarState extends State<BottomNavyBar> {
  int selectedIndex = 0;
  Color backgroundColor = Colors.white;

  List<NavigationItem> items = [
    NavigationItem(
      Icon(Icons.home),
      Text(
        'Home',
        style: TextStyle(fontSize: 14.0),
      ),
      Colors.deepPurpleAccent,
    ),
    NavigationItem(
      Icon(Icons.favorite_border),
      Text(
        'Favorite',
        style: TextStyle(fontSize: 14.0),
      ),
      Colors.pinkAccent,
    ),
    NavigationItem(
      Icon(Icons.search),
      Text(
        'Search',
        style: TextStyle(fontSize: 14.0),
      ),
      Colors.amber,
    ),
    NavigationItem(
      Icon(Icons.person_outline),
      Text(
        'Profile',
        style: TextStyle(fontSize: 14.0),
      ),
      Colors.cyan,
    ),
  ];

  Widget _buildItem(NavigationItem item, bool isSelected) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: double.maxFinite,
      width: isSelected ? 120.0 : 50.0,
      padding: isSelected
          ? EdgeInsets.only(
              left: 16.0,
              right: 16.0,
            )
          : null,
      decoration: isSelected
          ? BoxDecoration(
              color: item.color,
              borderRadius: BorderRadius.all(
                Radius.circular(50.0),
              ),
            )
          : null,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              IconTheme(
                data: IconThemeData(
                  size: 24.0,
                  color: isSelected ? backgroundColor : Colors.black,
                ),
                child: item.icon,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: isSelected
                    ? DefaultTextStyle.merge(
                        style: TextStyle(color: backgroundColor),
                        child: item.title,
                      )
                    : Container(),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.amber,
      height: 60.0,
      padding: EdgeInsets.only(left: 8.0, top: 4.0, bottom: 4.0, right: 8.0),
      decoration: BoxDecoration(
        color: backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 1.0,
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: items.map((item) {
          var itemIndex = items.indexOf(item);

          return GestureDetector(
            child: _buildItem(item, selectedIndex == itemIndex),
            onTap: () {
              setState(() {
                selectedIndex = itemIndex;
              });
            },
          );
        }).toList(),
      ),
    );
  }
}

class NavigationItem {
  final Icon icon;
  final Text title;
  final Color color;

  NavigationItem(this.icon, this.title, this.color);
}
