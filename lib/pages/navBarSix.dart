import 'package:flutter/material.dart';
import 'package:spincircle_bottom_bar/modals.dart';
import 'package:spincircle_bottom_bar/spincircle_bottom_bar.dart';

class NavBarSix extends StatefulWidget {
  @override
  _NavBarSixState createState() => _NavBarSixState();
}

class _NavBarSixState extends State<NavBarSix> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.deepOrangeAccent,
          title: Text("Spin Circle Nav"),
        ),
        body: SpinCircleBottomBarHolder(
          bottomNavigationBar: SCBottomBarDetails(
            iconTheme: IconThemeData(color: Colors.black45),
            activeIconTheme: IconThemeData(color: Colors.deepOrange),
            titleStyle: TextStyle(color: Colors.black45, fontSize: 11),
            activeTitleStyle: TextStyle(color: Colors.deepOrange, fontSize: 11, fontWeight: FontWeight.bold),
            bnbHeight: 80.0,
            backgroundColor: Colors.white,
            circleColors: [
              Colors.white,
              Colors.orange,
              Colors.redAccent,
            ],
            actionButtonDetails: SCActionButtonDetails(
              color: Colors.redAccent,
              icon: Icon(Icons.expand_less),
              elevation: 0.0,
            ),
            items: <SCBottomBarItem>[
              SCBottomBarItem(
                icon: Icons.verified_user,
                title: "Users",
                onPressed: () {},
              ),
              SCBottomBarItem(
                icon: Icons.supervised_user_circle,
                title: "Details",
                onPressed: () {},
              ),
              SCBottomBarItem(
                icon: Icons.notifications,
                title: "Notifications",
                onPressed: () {},
              ),
              SCBottomBarItem(
                icon: Icons.details,
                title: "New Data",
                onPressed: () {},
              ),
            ],
            circleItems: <SCItem>[
              SCItem(icon: Icon(Icons.add), onPressed: () {}),
              SCItem(icon: Icon(Icons.print), onPressed: () {}),
              SCItem(icon: Icon(Icons.map), onPressed: () {}),
            ],
          ),
          child: Container(
            color: Colors.grey.withAlpha(100),
            child: Center(
              child: Text("This is demo spin circle bottom menu"),
            ),
          ),
        ));
  }
}
