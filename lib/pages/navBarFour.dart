import 'package:flutter/material.dart';

class NavBarFour extends StatefulWidget {
  @override
  _NavBarFourState createState() => _NavBarFourState();
}

class _NavBarFourState extends State<NavBarFour> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _getNavBar(context),
    );
  }
}

_getNavBar(context) {
  return Stack(
    children: <Widget>[
      Positioned(
        bottom: 0,
        child: ClipPath(
          clipper: NavBarClipper(),
          child: Container(
            height: 60.0,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Colors.teal,
                  Colors.teal.shade900,
                ])),
          ),
        ),
      ),
      Positioned(
        bottom: 45.0,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _buildNavItem(Icons.bubble_chart, false),
            SizedBox(
              width: 1,
            ),
            _buildNavItem(Icons.landscape, true),
            SizedBox(
              width: 1,
            ),
            _buildNavItem(Icons.brightness_3, false),
          ],
        ),
      ),
      Positioned(
        bottom: 10.0,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              'Focus',
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(
              width: 1,
            ),
            Text(
              'Relax',
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(
              width: 1,
            ),
            Text(
              'Sleep',
              style: TextStyle(
                color: Colors.white.withOpacity(0.9),
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

_buildNavItem(IconData icon, bool active) {
  return CircleAvatar(
    radius: 30.0,
    backgroundColor: Colors.teal.shade900,
    child: CircleAvatar(
      radius: 25.0,
      backgroundColor:
          active ? Colors.white.withOpacity(0.9) : Colors.transparent,
      child: Icon(
        icon,
        color: active ? Colors.black : Colors.white.withOpacity(0.9),
      ),
    ),
  );
}

class NavBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    var x = size.width;
    var y = size.height;

    path.cubicTo(x / 12, 0, x / 12, 2 * y / 5, 2 * x / 12, 2 * y / 5);
    path.cubicTo(3 * x / 12, 2 * y / 5, 3 * x / 12, 0, 4 * x / 12, 0);
    path.cubicTo(5 * x / 12, 0, 5 * x / 12, 2 * y / 5, 6 * x / 12, 2 * y / 5);
    path.cubicTo(7 * x / 12, 2 * y / 5, 7 * x / 12, 0, 8 * x / 12, 0);
    path.cubicTo(9 * x / 12, 0, 9 * x / 12, 2 * y / 5, 10 * x / 12, 2 * y / 5);
    path.cubicTo(11 * x / 12, 2 * y / 5, 11 * x / 12, 0, x, 0);
    path.lineTo(x, y);
    path.lineTo(0, y);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
